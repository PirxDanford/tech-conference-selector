# Tech Conference Selector

Select interesting conferences from https://confs.tech/

## Prerequisites

A local installation of chrome or chromium is required.

Also either initialize the environment with `source init.sh` or if not on a unixoid system do:
- `go mod init tsel`
- `go get -u github.com/chromedp/chromedp`
- `go get -u github.com/PuerkitoBio/goquery`

## Usage

`go run tsel.go`

To tune your filter some flags are available:
- country default "Germany"
- topic default "devops"
- type default "hybrid"

Use `go run tsel.go --help` for more details.

Possible selection values are visible on https://confs.tech/ and are prone to change due to page changes as well as available conferences.