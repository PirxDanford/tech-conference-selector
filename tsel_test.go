package main

import (
	"testing"
)

// Test if URI will be constructed as expected
func Test_buildURI(t *testing.T) {

	got := buildURI("https://examp.le", map[string]string{"gn1": "gv1", "gn2": "gv2"})
	want := "https://examp.le?gn1=gv1&gn2=gv2"

	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
