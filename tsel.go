package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"log"
	"sort"
	"strings"
)

// this tool is supposed to read a filtered view from confs.tech and output it on the console, it could be extended to save it in html format for publishing the result on gitlab pages
func main() {
	// define the base location
	var baseURI string = "https://confs.tech"

	onlinetype := flag.String("type", "hybrid", "Please select if the conference should be hybrid, online or inPerson")
	countries := flag.String("country", "Germany", "Please select a country e.g. Germany")
	topics := flag.String("topic", "devops", "Please select a topic e.g. DevOps")
	// store := flag.String("target", "", "If required define a file to store the html output") // TODO: store output in a format which is nice to read on gitlab pages
	flag.Parse() // TODO: introduce error handling for empty or malformed input

	var getParam = map[string]string{
		"online":    *onlinetype, // consider to map inperson to inPerson to avoid typos
		"countries": *countries,  // consider to introduce multiple selections
		"topics":    *topics,     // consider to introduce multiple selections
		// consider to additionally include continents and offersSignLanguageOrCC parameter support
	}

	getURI := buildURI(baseURI, getParam) // fetch the URI with all parameters
	pageContents := getContents(getURI)   // fetch the contents as string
	events := getEntries(pageContents)    // parse the contents to an array
	// now output the final result
	for _, line := range events {
		fmt.Println(line)
	}
}

// for a base url and parameters create a single string URI and return it as string
func buildURI(base string, param map[string]string) string {
	returnURI := base

	// use an ordered array of keys to build the parameter chain
	keys := make([]string, 0, len(param))
	for k := range param {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var pos int = 0 // start count at 0
	for _, name := range keys {
		if pos == 0 {
			returnURI += "?" // add question mark delimiter if it is the first parameter to add
		} else {
			returnURI += "&" // add ampersand delimiter for multiple parameters
		}
		returnURI += name + "=" + param[name] // add parameter name and value
		pos++
	}
	return returnURI
}

// With headless chrome render the page contents for a URI
func getContents(uri string) (content string) {
	context, cancel := chromedp.NewContext(context.Background())
	defer cancel()
	var pageContents string
	if err := chromedp.Run(context,
		chromedp.Navigate(uri),
		chromedp.OuterHTML("html", &pageContents, chromedp.ByQuery),
	); err != nil {
		log.Fatal(err)
	}
	return pageContents
}

// with goquery parse only the relevant elements
func getEntries(content string) []string {
	entries := make([]string, 0)
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(content))
	if err != nil {
		log.Fatal(err)
	}

	doc.Find("dl").Each(func(entry int, event *goquery.Selection) { // each event is stored in a dl element
		description := ""
		event.Find("dd").Each(func(counter int, field *goquery.Selection) {
			switch counter { // we only need the first two dd elements for event name, location and date
			case 0:
				description += field.Find("a,p").Text()
			case 1:
				description += ", " + field.Find("a,p").Text()
			}
		})
		entries = append(entries, fmt.Sprintf("%s%d%s%s", "Event ", entry+1, ": ", description))
	})
	return entries
}

/* unused code snippet for storing the html if the flag store is enabled, not pretty, so needs work
if *store != "" {
	f, err := os.OpenFile(*store, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	dw := bufio.NewWriter(f)
	dw.WriteString(pageContents)
	dw.Flush()
	f.Close()
}*/
